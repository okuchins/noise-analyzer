#include <iostream>
#include <vector>
#include <sstream>

#include "draw.h"

using namespace std;

int main(int argc, char* argv[]) {
    if ( argc < 4 || (argc-2) % 2 != 0 ) {
        cout << "Usage:" << endl;
        cout << "\t app <outdir> <title1> <file1> .. <titleN> <fileN>" << endl;
        cout << endl;
        cout << "N must be less or equal 6" << endl;
        cout << "Data: /eos/atlas/atlascerngroupdisk/det-nsw-stgc/b191/" << endl;
        return 1;
    }

    vector<string> files;
    vector<string> titles;

    cout << "Argc = " << argc << endl;
    cout << ((argc-2) / 2) << endl;

    for (int i = 0; i < (argc-2) / 2; i += 1) {
        titles.push_back(argv[2*i+2]);
        files.push_back(argv[2*i+2+1]);
    }

    string outDir = argv[1];

    Draw(outDir, titles, files);

    return 0;
}
