#include "draw.h"

#include <TSystem.h>
#include <TDirectory.h>
#include <TFile.h>
#include <TTree.h>
#include <TH1F.h>
#include <THStack.h>
#include <TCanvas.h>
#include <TLegend.h>

#include <iostream>
#include <iomanip>

using namespace std;


struct FileInfo {
    int Triggers;
    string Title;
    int Color;
    string File;
    string Summary;
};



void Draw(const string& outDir, const vector<string>& titles, const vector<string>& files) {
    if (titles.size() != files.size()) {
        cerr << "ERR: Size of files differs from Size of titles" << endl;
        return;
    }
    if (files.size() > 6) {
        cerr << "ERR: files > 6" << endl;
        return;
    }
    {
        string dirCreate = "mkdir -p " + outDir;
        int r = gSystem->Exec(dirCreate.c_str());
        if (r != 0) {
            cerr << "ERR: Cannot create output directory" << endl;
            return;
        }
    }


    vector<int> colors{
        kRed,
        kOrange,
        kYellow,
        kGreen,
        kBlue,
        kViolet,
        kCyan,
        kAzure,
        kSpring,
        kTeal,
        kPink,
        kMagenta,
        kGray,
    };

    vector<string> histoname{
        // "HO_sFEB_hits_Layer_1",
        // "HO_sFEB_hits_Layer_2",
        // "HO_sFEB_hits_Layer_3",
        // "HO_sFEB_hits_Layer_4",
        // "IP_sFEB_hits_Layer_1",
        // "IP_sFEB_hits_Layer_2",
        // "IP_sFEB_hits_Layer_3",
        // "IP_sFEB_hits_Layer_4",
        "HO_pFEB_hits_Layer_1",
        "HO_pFEB_hits_Layer_2",
        "HO_pFEB_hits_Layer_3",
        "HO_pFEB_hits_Layer_4",
        "IP_pFEB_hits_Layer_1",
        "IP_pFEB_hits_Layer_2",
        "IP_pFEB_hits_Layer_3",
        "IP_pFEB_hits_Layer_4"
    };

    vector<FileInfo> infos;
    for (size_t i = 0; i < files.size(); i++) {
        cout << "Process [" << files[i] << "]" << endl;
        FileInfo info;
        info.File = files[i];
        info.Title = titles[i];

        cout << "\tTitle: " << info.Title << endl;

        {
            size_t idx = info.File.find_last_of(".");
            if (idx == string::npos) {
                cerr << "Invalid name: " << info.File << endl;
                return;
            }

            string sub = info.File.substr(0, idx);
            sub += "/summary_plots.root";
            info.Summary = sub;
        }
        cout << "\tSummary: " << info.Summary << endl;

        info.Color = colors[i];

        {
            TFile *file = TFile::Open(info.File.c_str(), "read");
            if (file == nullptr) {
                cerr << "Cannot open: " << info.File << endl;
                return;
            }
            TTree *tree;
            file->GetObject("nsw", tree);
            if (tree == nullptr) {
                cerr << "Cannot find 'nsw' in " << info.File << endl;
                return;
            }
            info.Triggers = tree->GetEntries();
            file->Close();
            delete file;
        }
        cout << "\tEntries: " << info.Triggers << endl;

        {
            TFile *file = TFile::Open(info.Summary.c_str(), "read");
            if (file == nullptr) {
                cerr << "Cannot open: " << info.File << endl;
                return;
            }
            for (const auto& histo: histoname) {
                TH1F *h;
                file->GetObject(histo.c_str(), h);
                if (h == nullptr) {
                    file->Close();
                    delete file;

                    cerr << "Cannot find histogram [" << histo << "] in " << info.Summary << endl;
                }
            }
            file->Close();
            delete file;
        }

        infos.push_back(info);
    }

    cout << endl;

    for (const auto& histo: histoname) {
        cout << "Histogram [" << histo << "]" << endl;
        TCanvas c("", "", 1200, 800);
        c.GetPad(0)->SetLogy();
        c.GetPad(0)->SetGridy();
        c.cd();

        TLegend legend(0.1,0.7,0.48,0.9);
        THStack hs("hs", histo.c_str());

        for (const auto &info: infos) {
            if (info.Triggers == 0) {
                continue;
            }

            TFile *file = TFile::Open(info.Summary.c_str(), "read");

            TH1F *h;
            file->GetObject(histo.c_str(), h);
            if (h == nullptr) {
                cerr << "Cannot find " << histo << endl;
                return;
            }

            h->SetDirectory(0); // make it orphan
            file->Close();
            delete file;

            for (int b = 0; b < h->GetNbinsX(); b++) {
                h->SetBinContent(b, h->GetBinContent(b) / info.Triggers / 0.000000025 / 8);
            }

            c.cd();
            h->SetFillColor(info.Color);
            h->SetMaximum(5*10000000);
            h->SetLineWidth(0);
            h->Print();

            hs.Add(h);
            gDirectory->Add(h); // new parent
            legend.AddEntry( h, info.Title.c_str());
        }

        cout << "!" << endl;
        hs.SetMinimum(0.1);
        hs.Draw("nostack");
        hs.GetXaxis()->SetTitle("Assembly ch");
        hs.GetYaxis()->SetTitle("noise rate [Hz]");

        legend.SetFillColor(0);
        legend.SetBorderSize(1);
        legend.Draw("");

        c.Print( (outDir + "/" + histo + ".png").c_str() );

        if (histo == histoname.front()) {
            c.Print( (outDir + "/" + "all.pdf(").c_str(), "pdf");
        } else if (histo == histoname.back()) {
            c.Print( (outDir + "/" + "all.pdf)").c_str(), "pdf");
        } else {
            c.Print( (outDir + "/" + "all.pdf").c_str(), "pdf");
        }
    }
}


//  25       data_test.1619606186._A11-noise-plus20-280421.daq.RAW._lb0000._191A-A11-swROD._0001.simple.root
//  930      data_test.1619606030._A11-noise-plus10-280421.daq.RAW._lb0000._191A-A11-swROD._0001.simple.root
// 8037      data_test.1619605875._A11-noise-nominal-280421.daq.RAW._lb0000._191A-A11-swROD._0001.simple.root
// 12136     data_test.1619606498._A11-noise-minus10-280421.daq.RAW._lb0000._191A-A11-swROD._0001.simple.root
// 0         data_test.1619606290._A11-noise-plus30-280421.daq.RAW._lb0000._191A-A11-swROD._0001.simple.root
// 13150     data_test.1619606573._A11-noise-minus20-280421.daq.RAW._lb0000._191A-A11-swROD._0001.simple.root
