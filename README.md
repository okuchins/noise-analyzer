# Noise Analyzer

Comparison plots for Noise runs at different thresholds.

## Build

```
setupATLAS
lsetup cmake
lsetup git
lsetup "root 6.22.06-x86_64-centos7-gcc8-opt"

git clone https://gitlab.cern.ch/okuchins/noise-analyzer.git
cd noise-analyzer

mkdir build
cmake ../
make
export PATH=$PATH:$(pwd)
```

## Usage

It works with files and directories created by `quicker.sh`. Currently, Data can be found here: `/eos/atlas/atlascerngroupdisk/det-nsw-stgc/b191/`


General usage:
```
noise_analyzer <output directory> <title1> <file1> <title2> <file2> ... <titleN> <fileN>
```
where N is any number between 1 and 6.


Example
```
cd /eos/atlas/atlascerngroupdisk/det-nsw-stgc/b191/A08/noise/07_05_2021/
noise_analyzer /tmp/results \
    "Threshold 10" data_test.1620380530._A08-noise-minus20.daq.RAW._lb0000._191A-A08-swROD._0001.simple.root \
    "Threshold 20" data_test.1620379530._A08-noise-minus10.daq.RAW._lb0000._191A-A08-swROD._0001.simple.root \
    "Threshold 30" data_test.1620378738._A08-noise-nominal.daq.RAW._lb0000._191A-A08-swROD._0001.simple.root \
    "Threshold 40" data_test.1620378878._A08-noise-plus10.daq.RAW._lb0000._191A-A08-swROD._0001.simple.root \
    "Threshold 50" data_test.1620379085._A08-noise-plus20.daq.RAW._lb0000._191A-A08-swROD._0001.simple.root \
    "Threshold 60" data_test.1620379255._A08-noise-plus30.daq.RAW._lb0000._191A-A08-swROD._0001.simple.root
```