#ifndef draw_h
#define draw_h

#include <vector>
#include <sstream>

void Draw(const std::string& outDir, const std::vector<std::string>& titles, const std::vector<std::string>& files);

#endif // draw_h