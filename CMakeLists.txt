cmake_minimum_required(VERSION 3.10)
project(noise_analyzer)

set(PROJECT
    noise_analyzer
)

set (CMAKE_CXX_STANDARD 11)
find_package(
    ROOT REQUIRED
    COMPONENTS
        Core
        Hist
        Tree
        RIO
        Physics
        MathCore
        Gui
        Gpad
        Graf
        TreePlayer
)


file(GLOB_RECURSE HEADERS
    "include/*.h"
    "include/*.inl"
    ${ROOT_INCLUDE_DIRS}
)

file(GLOB_RECURSE SOURCES
    "src/*.cpp"
)
message(STATUS  "ROOT Include: " ${ROOT_INCLUDE_DIRS})

add_executable(${PROJECT}
    ${SOURCES}
    ${HEADERS}
)

target_link_libraries(${PROJECT}
    ${ROOT_LIBRARIES}
)

include_directories(
    "include"
    ${ROOT_INCLUDE_DIRS}
)

set(APP_INSTALL_PATH ${CMAKE_INSTALL_PREFIX})
install(TARGETS ${PROJECT} DESTINATION ${APP_INSTALL_PATH})
